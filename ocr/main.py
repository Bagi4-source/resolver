import base64
import logging
from io import BytesIO
import cv2
import numpy as np
from PIL import Image, ImageEnhance
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import pytesseract

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S'
)
app = FastAPI()


class ImageData(BaseModel):
    image: str


class ImageRequest(ImageData):
    size: tuple = (500, 50)
    threshold: int = 100


class TextResponse(BaseModel):
    text: str


@app.post("/upscale_image", response_model=ImageData, tags=["OCR"])
def upscale_image(data: ImageRequest) -> ImageData:
    if 'base64,' not in data.image:
        raise HTTPException(422, "Image must be in base64")

    image = BytesIO(base64.b64decode(data.image.split('base64,')[-1]))

    image = Image.open(image)
    image = image.resize(data.size, resample=Image.Resampling.LANCZOS)

    res = Image.new('RGBA', size=image.size, color=(255, 255, 255))
    res.paste(image, mask=image, box=(0, 0))
    grayscale_image = res.convert('L')

    enhancer = ImageEnhance.Contrast(grayscale_image)
    contrast_image = enhancer.enhance(2.0)

    opencv_image = np.array(contrast_image)

    cv_image = cv2.cvtColor(opencv_image, cv2.COLOR_RGB2BGR)
    _, binary_image = cv2.threshold(cv_image, data.threshold, 255, cv2.THRESH_BINARY)

    retval, buffer = cv2.imencode('.png', binary_image)
    encoded_image = base64.b64encode(buffer).decode()
    return ImageData(image=f"data:image/png;base64,{encoded_image}")


@app.post("/image_to_text", response_model=TextResponse, tags=["OCR"])
def image_to_text(data: ImageData, lang: str = 'chi_sim') -> TextResponse:
    if 'base64,' not in data.image:
        raise HTTPException(422, "Image must be in base64")
    image = BytesIO(base64.b64decode(data.image.split('base64,')[-1]))
    image = Image.open(image)
    return TextResponse(text=pytesseract.image_to_string(image, lang=lang, config='--psm 12 --oem 3'))
