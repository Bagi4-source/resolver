import logging
from typing import Any
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S'
)
app = FastAPI()


class TranslationData(BaseModel):
    fromLang: str
    toLang: str


class TranslationResponse(TranslationData):
    translation: str


class TranslationRequest(TranslationData):
    text: str


class Translator(BaseModel):
    tokenizer: Any
    model: Any

    def __init__(self, path):
        super().__init__(
            tokenizer=AutoTokenizer.from_pretrained(path),
            model=AutoModelForSeq2SeqLM.from_pretrained(path)
        )

        logging.debug(f"{path} is loaded!")


translators = {
    "zh->en": Translator(path="models/zhEn"),
    "en->ru": Translator(path="models/enRu"),
    "ru->en": Translator(path="models/ruEn2"),
}


@app.post("/translate", response_model=TranslationResponse, tags=["Translator"])
def translate(data: TranslationRequest):
    logging.debug(f"{data}")

    code = f"{data.fromLang}->{data.toLang}"
    translator = translators.get(code)
    if not translator:
        logging.warning(f"{code} not found!")
        raise HTTPException(404, detail=f"{code} not found!")

    translator_tokenizer = translator.tokenizer
    translator_model = translator.model

    data.text.encode("utf-8")
    input_ids = translator_tokenizer.encode(data.text, return_tensors="pt")
    outputs = translator_model.generate(input_ids)
    decoded = translator_tokenizer.decode(outputs[0], skip_special_tokens=True)

    result = TranslationResponse(translation=decoded, fromLang=data.fromLang, toLang=data.toLang)

    logging.debug(f"{result}")
    return result